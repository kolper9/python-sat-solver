import sys
import random 
import time
from collections import deque 


# Store best result of gsat and truth assigment of this result 
class bestVariables(object):
	def __init__(self):
		self.Sat = 0
		self.bestVariables=[]
	#add only if adding solution is better than current 
	def add( self,variable,Sat ):
		if self.Sat<Sat:
			self.Sat=Sat
			self.bestVariables=[]
			self.bestVariables.append(variable)
		elif self.Sat==Sat:
			self.bestVariables.append(variable)
		else:
			return



# Load formule in dimasc format
def loadCnf(file):
	with open(file, 'r') as fi:
		data = fi.readlines()
		numberClausules=0
		numberVariables=0

		clausules=[]
		for ln in data:
			if ln.startswith("w"):
				continue
			if ln.startswith("c"):
				continue
			if ln.startswith("p"):
				numberVariables=int(ln.split()[2])
				numberClausules=int(ln.split()[3])
				continue
			if ln.startswith("%"):
				break
			oneClausule=[]
			for variable in ln.split():
				if not int(variable) == 0:
					oneClausule.append(int(variable))
			clausules.append(oneClausule)
	return numberClausules,numberVariables,clausules


def randomAssingnment(numberVariables):
	truthAssignment=[]
	for x in range(0,numberVariables):
		truthAssignment.append(random.randint(0,1))
	return truthAssignment

def numberSatisfied(clausules,truthAssignment):
	Sat=0
	for c in clausules:
		for v in c:
			if v>0:

				if  truthAssignment[v-1]:
					Sat+=1
					break
			else:
				if not truthAssignment[(-v)-1]:
					Sat+=1
					break
	return Sat


def numberSatisfiedClausules(clausules,truthAssignment):
	Sat=0
	clausulesSatisfied=[]
	wasSat=False


	for c in clausules:
		for v in c:
			if v>0:
				if  truthAssignment[v-1]:
					Sat+=1
					clausulesSatisfied.append(True)
					wasSat=True
					break
			else:
				if not truthAssignment[(-v)-1]:
					Sat+=1
					wasSat=True
					clausulesSatisfied.append(True)
					break
		if wasSat!=True:
			clausulesSatisfied.append(False)
		wasSat=False	
	return clausulesSatisfied


#return number od satisfied clausules with invert watched variable
def numberSatisfiedAllWatched(clausules,truthAssignment,watchedVariable,variable,clausulesSatisfied):
	Sat=0
	UnSat=0
	wasSat=False

	for c in watchedVariable:
		for v in clausules[c]:
			if v>0:
				if  truthAssignment[v-1]:
					if not clausulesSatisfied[c]:
						Sat+=1
						
					wasSat=True
					break
			else:
				if not truthAssignment[(-v)-1]:
					if not clausulesSatisfied[c]:
						Sat+=1

					wasSat=True
					break

		if wasSat!=True:
			if clausulesSatisfied[c]:
				UnSat+=1
		wasSat=False	

	return Sat-UnSat



#return number od satisfied clausules with one watched variable
def numberSatisfiedWatched(clausules,truthAssignment,watchedVariable,variable,clausulesSatisfied):
	Sat=0
	UnSat=0
	wasSat=False

	for c in watchedVariable:
		for x in range (0,len(clausules[c])):
			v=clausules[c][x]
			if v>0:
				if  truthAssignment[v-1]:

					if not clausulesSatisfied[c]:	
						Sat+=1
						
					
					wasSat=True
					break
			else:
				if not truthAssignment[(-v)-1]:


					
					if not clausulesSatisfied[c]:	
						Sat+=1
						

					wasSat=True
					break
		if wasSat!=True:
			UnSat+=1
		wasSat=False	

	return Sat-UnSat


#create inverted watched variable
def creatWatched(clausules,numberVariables):
	watchedVariables=[]
	for numberVariables in range(0,numberVariables):
		watchedVariables.append([])
	
	for x in range(0,len(clausules)):
		for v in clausules[x]:	
			if v>0:
				watchedVariables[(v-1)].append(x)
			else:
				watchedVariables[(-v)-1].append(x)
	return watchedVariables


#create 2 watched variable list
def creat2Watched(clausules,numberVariables):
	watchedVariables=[]
	for numberVariables in range(0,numberVariables):
		watchedVariables.append([])
	
	for x in range(0,len(clausules)):
		for i in range(0,2):	
			v=clausules[x][i]
			if v>0:
				watchedVariables[(v-1)].append((x,i,1^i))   # number of clausule ,positoin in clausule, next element in clausule
			else:
				watchedVariables[(-v)-1].append((x,i,1^i)) # number of clausule ,positoin in clausule, next element in clausule
	return watchedVariables

#creat list od one watched variavble
#return number od sat clauseles and 

def creatOneWatched(clausules,numberVariables,truthAssignment,clausulesSatisfied):
	watchedVariables=[]
	
	for numberVariables in range(0,numberVariables):
		watchedVariables.append([])
	
	wasSat=False
	Sat=0

	for x in range(0,len(clausules)):
		
		for v in clausules[x]:
			
			if v>0:
				if  truthAssignment[v-1]:
					Sat+=1
					
					wasSat=True
					watchedVariables[(v-1)].append(x)
					clausulesSatisfied[x]=True
					break
			else:

				if not truthAssignment[(-v)-1]:
					Sat+=1
					
					wasSat=True
					
					clausulesSatisfied[x]=True
					watchedVariables[(-v)-1].append(x)
					break

		if wasSat!=True:
			clausulesSatisfied[x]=False
			for v in clausules[x]:
				if v>0:
					watchedVariables[(v-1)].append(x)
				else:
					watchedVariables[(-v)-1].append(x)
			


		wasSat=False	
	
	return Sat,watchedVariables,clausulesSatisfied


# update list of watched variable 
def creatOneWatched_fromWatched(clausules,numberVariables,truthAssignment,clausulesSatisfied,watchedVariables,variable):
	wasSat=False
	Sat=0
	UnSat=0

	w=watchedVariables[variable].copy()
	for c in w:
		for x in range (0,len(clausules[c])):
			v=clausules[c][x]
			if v>0:
				if  truthAssignment[v-1]:

					if not clausulesSatisfied[c]:	
					
						clausulesSatisfied[c]=True
						Sat+=1
						for x in range (0,len(clausules[c])):
							v=clausules[c][x]
							
							if v-1==variable or (-v)-1==variable:
								continue
							if v>0:
								watchedVariables[v-1].remove(c)
								
							else:
								watchedVariables[(-v)-1].remove(c)
								
					
					else:
						
						watchedVariables[variable].remove(c)
						watchedVariables[(v-1)].append(c)
						
					wasSat=True
					break
			else:
				if not truthAssignment[(-v)-1]:
					
					if not clausulesSatisfied[c]:	
						Sat+=1
						clausulesSatisfied[c]=True

						for x in range (0,len(clausules[c])):
							v=clausules[c][x]
							if v-1==variable or (-v)-1==variable:
								continue
							if v>0:
							
								watchedVariables[v-1].remove(c)
								
							else:
							
								watchedVariables[(-v)-1].remove(c)
								
					
					else:
					
						watchedVariables[variable].remove(c)
						watchedVariables[(-v)-1].append(c)
							
						

					wasSat=True
					break




		if wasSat!=True:
		
			clausulesSatisfied[c]=False

			UnSat+=1
			for x in range (0,len(clausules[c])):
				v=clausules[c][x]
				if v-1==variable or (-v)-1==variable:
					continue
				if v>0:
					watchedVariables[v-1].append(c)
					
				else:
					watchedVariables[(-v)-1].append(c)
					
			
		wasSat=False


	return Sat-UnSat




def GSAT(clausules,numberIterations,numberFlips,numberVariables,numberClausules):
	bestSatAllIteration=0
	besttruthAssignment=[]
	watchedVariables=[]

	for ite in range(0,numberIterations):
		truthAssignment=randomAssingnment(numberVariables)

		for flips in range(0,numberFlips):
			best=bestVariables()
			for var in range(0,numberVariables):

				truthAssignment[var]=not truthAssignment[var]
				best.add(var,numberSatisfied(clausules,truthAssignment))
				truthAssignment[var]=not truthAssignment[var]


			if best.Sat==len(clausules):
				truthAssignment[best.bestVariables[0]]= not truthAssignment[best.bestVariables[0]]
				return best.Sat,truthAssignment
			else:
				flip = random.randint(0,len(best.bestVariables))

				truthAssignment[best.bestVariables[flip]]= not truthAssignment[best.bestVariables[flip]]
				if bestSatAllIteration<=best.Sat:
					bestTruthAssignment=truthAssignment
					bestSatAllIteration=best.Sat

	return bestSatAllIteration,bestTruthAssignment



def GSATW(clausules,numberIterations,numberFlips,numberVariables,numberClausules):
	bestSatAllIteration=0
	besttruthAssignment=[]
	watchedVariables=[]
	watchedVariables=creatWatched(clausules,numberVariables)
		
	for ite in range(0,numberIterations):
		truthAssignment=randomAssingnment(numberVariables)
		addToSat=numberSatisfied(clausules,truthAssignment)


		for flips in range(0,numberFlips):

			clausulesSatisfied=numberSatisfiedClausules(clausules,truthAssignment)
			best=bestVariables()
			for var in range(0,numberVariables):
				
				truthAssignment[var]=not truthAssignment[var]
				best.add(var, numberSatisfiedAllWatched(clausules,truthAssignment,watchedVariables[var],var,clausulesSatisfied)+addToSat)
				truthAssignment[var]=not truthAssignment[var]




			if best.Sat==len(clausules):
				truthAssignment[best.bestVariables[0]]= not truthAssignment[best.bestVariables[0]]
				return best.Sat,truthAssignment
			else:
				flip = random.randint(0,len(best.bestVariables)-1)
				truthAssignment[best.bestVariables[flip]]= not truthAssignment[best.bestVariables[flip]]
				addToSat=best.Sat
				if bestSatAllIteration<=best.Sat:
					bestTruthAssignment=truthAssignment
					bestSatAllIteration=best.Sat

	return bestSatAllIteration,bestTruthAssignment
			


def GSATOW(clausules,numberIterations,numberFlips,numberVariables,numberClausules):
	bestSatAllIteration=0
	besttruthAssignment=[]
	watchedVariables=[]
	clausulesSatisfied=[]


	
	for x in range(0,numberClausules):
		clausulesSatisfied.append(False)

	


	for ite in range(0,numberIterations):
		truthAssignment=randomAssingnment(numberVariables)
		addToSat,watchedVariables,clausulesSatisfied=creatOneWatched(clausules,numberVariables,truthAssignment,clausulesSatisfied)

		for flips in range(0,numberFlips):
			best=bestVariables()
			
			for var in range(0,numberVariables):
				
				truthAssignment[var]=not truthAssignment[var]
				
				th=numberSatisfiedWatched(clausules,truthAssignment,watchedVariables[var],var,clausulesSatisfied)
				best.add(var,th+addToSat)

				truthAssignment[var]=not truthAssignment[var]


			if best.Sat==len(clausules):
				truthAssignment[best.bestVariables[0]]= not truthAssignment[best.bestVariables[0]]
				return best.Sat,truthAssignment
			else:
				flip = random.randint(0,len(best.bestVariables)-1)
				truthAssignment[best.bestVariables[flip]]= not truthAssignment[best.bestVariables[flip]]
				addToSat=best.Sat
				creatOneWatched_fromWatched(clausules,numberVariables,truthAssignment,clausulesSatisfied,watchedVariables,best.bestVariables[flip])
				if bestSatAllIteration<=best.Sat:
					bestTruthAssignment=truthAssignment
					bestSatAllIteration=best.Sat

	return bestSatAllIteration,bestTruthAssignment
	
def backtracking(clausules,numberVariables,numberClausules,numVar,truthAssignment,bestSat,besttruthAssignment):
	
	if numVar<numberVariables:
		
		truthAssignment[numVar]=False
		sat,truth=backtracking(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment)
		if bestSat<sat:
			bestSat=sat
			besttruthAssignment=truth

		truthAssignment[numVar]=True
		sat,truth=backtracking(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment)
		if bestSat<sat:
			bestSat=sat
			besttruthAssignment=truth
	else:
		return numberSatisfied(clausules, truthAssignment),truthAssignment

	return	bestSat,besttruthAssignment




		

def unitProp(clausules,truthAssignment,numberVariables):
	var_Pure=[]		#0 in none clousule 1 positive form 2 negativ form 3 positive negative form 
	for c in range(0,numberVariables):
		var_Pure.append(0)
	numTruth=0
	prop=True
	unit=0

	isUniq=False
	non_Sat=False
	false_Sat=False

	deb=[]
	while prop:
		prop=False
		for c in clausules:
			non_Sat=True
			isUniq=False
			wasProp=True
			wasOnlyFalse=True
			for v in c:
				if v>0:
					if truthAssignment[(v-1)]==-1:
						if var_Pure[v-1]:
							var_Pure[v-1]=3
						else:
							var_Pure[v-1]=1
						non_Sat=False


						if not isUniq:
							unit=v
							
							isUniq=True
							
						else:
							wasProp=False
											
					else:
						if truthAssignment[(v-1)]:
							wasOnlyFalse=False
							non_Sat=False
						
							

				else:
					if truthAssignment[(-v)-1]==-1:
						if var_Pure[(-v)-1]:
							var_Pure[(-v)-1]=3
						else:
							var_Pure[(-v)-1]=2
						
						non_Sat=False
						if not isUniq:
							unit=v
							isUniq=True
							
							
						else:
							wasProp=False	
							

					else:
						if not truthAssignment[(-v)-1]:
							non_Sat=False
							wasOnlyFalse=False
						
			if non_Sat:
				return -1
			
			
				
			if 	isUniq and wasProp and wasOnlyFalse:
				prop=True
	
					
				numTruth+=1
				if unit>0:
					truthAssignment[(unit-1)]=True
				else:
					truthAssignment[(-unit)-1]=False


	for v in var_Pure:
	 	if v==1:
	 		truthAssignment[v]=True
	 	elif v==2:
	 		truthAssignment[v]=False
	return numTruth



def truthToPos(truth):
	if truth>0 :
		return (truth)-1
	else: 
		return (-truth)-1




def unitProp_watched(clausules,truthAssignment,numberVariables,variable,watchedVariables):

	
	non_Sat=False
	
	newLit=False

	
	#queue for new variable to solve variable and watch list
	q = deque()
	q.append((variable,watchedVariables[variable].copy()))
	
	 
	#while is unit prapagate with new unit propagation add new item to queue 
	while len(q):
		
		
		it_var,loop= q.pop()
		
		for c in loop:
			
			v=clausules[c[0]][c[1]]
			isTrue=False

			# fisrt watch true or false
			if v>0:
				if truthAssignment[(v-1)]==1:
					isTrue=True
					
				else:
					isTrue=False

			else:
				if truthAssignment[(-v)-1]==0:
					isTrue=True
				
				else:
					isTrue=False


				
			
				
				
			#fisrt watch false
			if not isTrue:
					
					secondWatch=clausules[c[0]][c[2]]
					if(truthAssignment[truthToPos(secondWatch)]==-1): # Second watch litteral is not resolve
						
							newLit=False
							newWatch=3-c[1]-c[2]
							posNewLit=clausules[c[0]][newWatch]
							
							if truthAssignment[truthToPos(posNewLit)]==-1: #set new watch litteral
						
								newLit=True
								watchedVariables[truthToPos(posNewLit)].append((c[0],newWatch,c[2]))
								
								watchedVariables[it_var].remove(c)
							
								
							else:	
								if posNewLit > 0:
									if truthAssignment[posNewLit-1]==1:
										
										newLit=True  #new literal true
										
								else:
									if truthAssignment[(-posNewLit)-1]==0:
									
										newLit=True  #new literal true
										

							if not newLit: # one non resolve lit one true or false one false == UNIT PROPAGATION
							
								unitV=clausules[c[0]][c[2]]
							

							  #UNIT PROPAGATION
								if unitV>0:
								
									truthAssignment[(unitV-1)]=True
									q.append(((unitV)-1,watchedVariables[(unitV)-1].copy()))
								else:
									truthAssignment[(-unitV)-1]=False
									q.append(((-unitV)-1,watchedVariables[(-unitV)-1].copy()))
									
								prop=True
							
						
						
					else: 
					 
						isSat=False
						if secondWatch>0:
							if truthAssignment[secondWatch-1]==1:
								isSat=True    #second watched is sat
							
						else:
							if truthAssignment[(-secondWatch)-1]==0:
								isSat=True	  #second watched is sat
								
						
							
				
						if(not isSat):	#second not sat 
								non_Sat=True #Cl not sat
								

								newWatch=3-c[1]-c[2]
								posNewLit=clausules[c[0]][newWatch]
							

								if truthAssignment[truthToPos(posNewLit)]==-1: # New literal not resolve UNIT PROP  
									non_Sat=False
									
									
									unitV=clausules[c[0]][newWatch]
									

							 		 #UNIT PROPAGATION
									if unitV>0:
										truthAssignment[(unitV-1)]=True
										q.append(((unitV)-1,watchedVariables[(unitV)-1].copy()))
										

									else:
										truthAssignment[(-unitV)-1]=False
										q.append(((-unitV)-1,watchedVariables[(-unitV)-1].copy()))
										

								else:
									if posNewLit>0:
										if truthAssignment[(posNewLit-1)]==True:  # FFT  new lit is sat
											non_Sat=False
											
									else:
										if truthAssignment[(-posNewLit)-1]==False: # FFT  new lit is sat
											non_Sat=False
											
									prop=True
								


									

								if  non_Sat: # not SAt  FFF all not sat 
									return -1


	return 0









def DPLL(clausules,numberVariables,numberClausules,numVar,truthAssignment,bestSat,besttruthAssignment,numTruth):

	


	if numTruth<numberVariables:
		
		retUit=unitProp(clausules,truthAssignment,numberVariables)
		if retUit==-1:
			return	bestSat,besttruthAssignment
		else:	
			numTruth+=retUit
		

		if truthAssignment[numVar]==-1:
			truthAssignment[numVar]=False
			sat,truth=DPLL(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment,numTruth+1)
			if bestSat<sat:
				bestSat=sat
				besttruthAssignment=truth
			if(bestSat==numberClausules):
				return	bestSat,besttruthAssignment


			truthAssignment[numVar]=True
			sat,truth=DPLL(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment,numTruth+1)
			if bestSat<sat:
				bestSat=sat
				besttruthAssignment=truth
			if(bestSat==numberClausules):
				return	bestSat,besttruthAssignment
			
		else:
			sat,truth=DPLL(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment,numTruth)

		if bestSat<sat:
				bestSat=sat
				besttruthAssignment=truth
		if(bestSat==numberClausules):
				return	bestSat,besttruthAssignment
	else:
		return numberSatisfied(clausules, truthAssignment),truthAssignment

	return	bestSat,besttruthAssignment




def DPLL_Watched(clausules,numberVariables,numberClausules,numVar,truthAssignment,bestSat,besttruthAssignment,numTruth,watchedVariables):
	
	if numVar<numberVariables:
		if numVar!=0:
			
			retUit=unitProp_watched(clausules,truthAssignment,numberVariables,numVar-1,watchedVariables)
		
			if retUit==-1:
				return	bestSat,besttruthAssignment
			else:	
				numTruth+=retUit


		while  numVar<numberVariables and truthAssignment[numVar]!=-1:
			numVar+=1
			
		if numVar>=numberVariables:
			return numberSatisfied(clausules, truthAssignment),truthAssignment

		if truthAssignment[numVar]==-1:

			truthAssignment[numVar]=False
			
				
			sat,truth=DPLL_Watched(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment,numTruth,watchedVariables)
			
			if bestSat<sat:
				bestSat=sat
				besttruthAssignment=truth
			if(bestSat==numberClausules):
				return	bestSat,besttruthAssignment


			truthAssignment[numVar]=True
				
			sat,truth=DPLL_Watched(clausules,numberVariables,numberClausules,numVar+1,truthAssignment.copy(),bestSat,besttruthAssignment,numTruth,watchedVariables)

			if bestSat<sat:
				bestSat=sat
				besttruthAssignment=truth
			if(bestSat==numberClausules):
				return	bestSat,besttruthAssignment
				
		
	
			
	else:
		return numberSatisfied(clausules, truthAssignment),truthAssignment




	return	bestSat,besttruthAssignment

def main():
	clausules=[]

	numberClausules,numberVariables,clausules=loadCnf(sys.argv[1])
	truthAssignment=[]
	print(numberClausules)
	print(numberVariables)
	for x in range(0,numberVariables):
		truthAssignment.append(-1)

	#satClausules=[ 0 for i in range(0,numberClausules)]
	#bestSat,truthAssignment=GSATOW(clausules,200,100,numberVariables,numberClausules)

	#watchedVariables=creat2Watched(clausules,numberVariables)
	#bestSat,truthAssignment=DPLL_Watched(clausules,numberVariables,numberClausules,0,truthAssignment,0,[],0,watchedVariables)

	bestSat,truthAssignment=DPLL(clausules,numberVariables,numberClausules,0,truthAssignment,0,[],0)


	if(bestSat==numberClausules):
		print("Satisfied best solution" + str(truthAssignment) )
		print(str(numberSatisfied(clausules, truthAssignment)))

	else:
		print("Unsatisfied number of sat clausules "  + str(bestSat) + " best solution " + str(truthAssignment) )



if __name__ == "__main__": 
    main()
					


