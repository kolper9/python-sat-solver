# Python SAT solver

Implements 4 types of SAT solvers GSAT, GSAT with one watched literal, DPLL, and DPLL with two watched literals. Input is in CNF.